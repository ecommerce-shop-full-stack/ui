import camera1 from "../images/products/camera.png";
import camera2 from "../images/products/camera2.png";
import smartwatch1 from "../images/products/smart-watch.png";
import smartwatch2 from "../images/products/smart-watch2.png";
import laptop1 from "../images/products/laptop.png";
import laptop2 from "../images/products/laptop2.png";
import tivi1 from "../images/products/tivi.png";
import tivi2 from "../images/products/tivi2.png";
import speaker1 from "../images/products/speaker.png";
import speaker2 from "../images/products/speaker2.png";
import playstation1 from "../images/products/playstation.png";
import playstation2 from "../images/products/playstation2.png";

const FCollectionData = [
  {
    imgUrl1: camera1,
    imgUrl2: camera2,
    name: "Havels",
    title: "Camera",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "100",
    star: 3,
  },
  {
    imgUrl1: smartwatch1,
    imgUrl2: smartwatch2,
    name: "Apple",
    title: "Watch",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "400",
    star: 4,
  },
  {
    imgUrl1: laptop1,
    imgUrl2: laptop2,
    name: "MSI",
    title: "Laptop",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "1.100",
    star: 5,
  },
  {
    imgUrl1: speaker1,
    imgUrl2: speaker2,
    name: "JBL",
    title: "Speaker",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "550",
    star: 3,
  },
  {
    imgUrl1: playstation1,
    imgUrl2: playstation2,
    name: "SONY",
    title: "Play Station 5",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "800",
    star: 4,
  },
  {
    imgUrl1: tivi1,
    imgUrl2: tivi2,
    name: "SONY",
    title: "Tivi",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.",
    price: "1.800",
    star: 5,
  },
];

export default FCollectionData;
