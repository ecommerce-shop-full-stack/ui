import phone from "../images/products/phone.png";
import headphone from "../images/products/accessories.png";
import tivi from "../images/products/tivi.png";
import laptop from "../images/products/laptop.png";
import playstation from "../images/products/playstation.png";
import smartwatch from "../images/products/smart-watch.png";

const SpecialData = [
  {
    imgUrl: phone,
    brand: "Haveks",
    title: "iPhone 14 Pro Max 512G",
    star: 4,
    discountPrice: 750,
    price: 1000,
    quantity: 10,
  },
  {
    imgUrl: headphone,
    brand: "Apple",
    title: "AirPods Pro Gen 5",
    star: 3,
    discountPrice: 350,
    price: 400,
    quantity: 10,
  },
  {
    imgUrl: tivi,
    brand: "SONY",
    title: "Sony LCD Ultra 4K",
    star: 5,
    discountPrice: 1700,
    price: 1800,
    quantity: 10,
  },
  {
    imgUrl: laptop,
    brand: "MSI",
    title: "MSI Alien Ware Hard Core",
    star: 3,
    discountPrice: 1750,
    price: 2000,
    quantity: 10,
  },
  {
    imgUrl: playstation,
    brand: "SONY",
    title: "Sony Play Station 5 White",
    star: 5,
    discountPrice: 1300,
    price: 1500,
    quantity: 10,
  },
  {
    imgUrl: smartwatch,
    brand: "Apple",
    title: "Apple Watch 4 Black",
    star: 3,
    discountPrice: 320,
    price: 400,
    quantity: 10,
  },
];
export default SpecialData;
