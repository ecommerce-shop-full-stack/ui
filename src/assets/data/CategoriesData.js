import camera from "../images/products/camera.png";
import smartwatch from "../images/products/smart-watch.png";
import laptop from "../images/products/laptop.png";
import tivi from "../images/products/tivi.png";
import speaker from "../images/products/speaker.png";
import playstation from "../images/products/playstation.png";
import phone from "../images/products/phone.png";
import keyboard from "../images/products/keyboard.png";
import accessories from "../images/products/accessories.png";
import headphone from "../images/products/headphone.png";

const CategoriesData = [
  {
    name: "Cameras",
    quantity: "10 Items",
    imgUrl: camera,
  },
  {
    name: "Watches",
    quantity: "10 Items",
    imgUrl: smartwatch,
  },
  {
    name: "Laptops",
    quantity: "10 Items",
    imgUrl: laptop,
  },
  {
    name: "Tivi",
    quantity: "10 Items",
    imgUrl: tivi,
  },
  {
    name: "Phones",
    quantity: "10 Items",
    imgUrl: phone,
  },
  {
    name: "Keyboard",
    quantity: "10 Items",
    imgUrl: keyboard,
  },
  {
    name: "Speaker",
    quantity: "10 Items",
    imgUrl: speaker,
  },
  {
    name: "Accessories",
    quantity: "10 Items",
    imgUrl: accessories,
  },
  {
    name: "Headphones",
    quantity: "10 Items",
    imgUrl: headphone,
  },
  {
    name: "Game",
    quantity: "10 Items",
    imgUrl: playstation,
  },
];

export default CategoriesData;
