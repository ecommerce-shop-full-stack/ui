import phone from "../images/products/phone.png";
import smartwatch from "../images/products/smart-watch.png";
import laptop from "../images/products/laptop.png";
import speaker from "../images/products/speaker.png";

const PopularData = [
  {
    top: "big green",
    middle: "Smart Watch Series 7",
    bottom: "From $399 or $16.62/mo. for 24 mo*",
    imgUrl: smartwatch,
  },
  {
    top: "studio display",
    middle: "600 nits of brightness",
    bottom: "27-inch 5K retina display",
    imgUrl: laptop,
  },
  {
    top: "smartphones",
    middle: "smartphone 14 pro",
    bottom: "Now in green. From $999 or $41.62/mo. for 24 mo.",
    imgUrl: phone,
  },
  {
    top: "home speakers",
    middle: "room-filling sound.",
    bottom: "From $699 or $58.25/mo for 12mo",
    imgUrl: speaker,
  },
];

export default PopularData;
