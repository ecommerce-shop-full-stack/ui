import phone from "../images/products/phone.png";
import laptop from "../images/products/laptop.png";
import tivi from "../images/products/tivi.png";
import speaker from "../images/products/speaker.png";

const CompareProductData = [
  {
    imgUrl: phone,
    title: "Honor T1 7.0 1GB RAM 8GB ROM 7 Inch",
    price: 100,
    brand: "Haviks",
    type: "Phone",
    sku: "SKU033",
    availability: "In Stock",
    firstColor: "#EC11C3",
    secondColor: "#2211C3",
    thirdColor: "#22E2C3",
    size: ["S", " ", "M", " ", "L"],
  },
  {
    imgUrl: laptop,
    title: "Lenovo 16GB RAM 1TB ROM 16 Inch",
    price: 2100,
    brand: "Lenovo",
    type: "Laptop",
    sku: "SKU043",
    availability: "In Stock",
    firstColor: "#225D2E",
    secondColor: "#DE5D2E",
    thirdColor: "#005DD4",
    size: ["S", " ", "M", " ", "L"],
  },
  {
    imgUrl: tivi,
    title: "Samsung Oled Tivi 60 Inch 4K Ultra",
    price: 1100,
    brand: "Haviks",
    type: "Samsung",
    sku: "SKU013",
    availability: "In Stock",
    firstColor: "#00FCD4",
    secondColor: "#E9FCD4",
    thirdColor: "#E9FC2F",
    size: ["S", " ", "M", " ", "L"],
  },
  {
    imgUrl: speaker,
    title: "JBL Speaker 5.0 Real Dynamic Sound",
    price: 600,
    brand: "JBL",
    type: "Speaker",
    sku: "SKU003",
    availability: "In Stock",
    firstColor: "#E9FCF2",
    secondColor: "#77FCF2",
    thirdColor: "#7782F2",
    size: ["S", " ", "M", " ", "L"],
  },
];

export default CompareProductData;
