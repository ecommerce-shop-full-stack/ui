import hp from "../images/brands/hp.png";
import lg from "../images/brands/lg.png";
import samsung from "../images/brands/samsung.png";
import msi from "../images/brands/msi.png";
import ibm from "../images/brands/ibm.png";
import lenovo from "../images/brands/lenovo.png";
import xiaomi from "../images/brands/xiaomi.png";

const BrandData = [
  {
    imgUrl: hp,
    name: "HP",
  },
  {
    imgUrl: lg,
    name: "LG",
  },
  {
    imgUrl: samsung,
    name: "SAMSUNG",
  },
  {
    imgUrl: msi,
    name: "MSI",
  },
  {
    imgUrl: ibm,
    name: "IBM",
  },
  {
    imgUrl: lenovo,
    name: "LENOVO",
  },
  {
    imgUrl: xiaomi,
    name: "XIAOMI",
  },
];
export default BrandData;
