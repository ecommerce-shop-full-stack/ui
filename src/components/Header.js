import React from "react";
import { NavLink, Link } from "react-router-dom";
import { BsSearch } from "react-icons/bs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index";
import {
  faCartPlus,
  faCodeCompare,
  faUser,
  faHeart,
  faBars,
} from "@fortawesome/free-solid-svg-icons";

const Header = () => {
  return (
    <>
      <header className="header-top-strip py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-6">
              <p className="text-white mb-0">
                Free Shipping Over $100 & Free Returns
              </p>
            </div>
            <div className="col-6">
              <p className="text-end text-white mb-0">
                Hotline:
                <a className="text-white" href="tel:+84 909 054 830">
                  +84 909 054 830
                </a>
              </p>
            </div>
          </div>
        </div>
      </header>
      <header className="header-upper py-3">
        <div className="container-xxl">
          <div className="row align-items-center">
            <div className="col-2">
              <h2>
                <Link to="/" className="text-white">
                  Digitic.
                </Link>
              </h2>
            </div>
            <div className="col-5">
              <div className="input-group">
                <input
                  type="text"
                  className="form-control py-2"
                  placeholder="Search Product Here..."
                  aria-label="Search Product Here..."
                  aria-describedby="basic-addon2"
                />
                <span className="input-group-text p-3" id="basic-addon2">
                  <BsSearch className="fs-6" />
                </span>
              </div>
            </div>
            <div className="col-5">
              <div className="header-upper-links d-flex align-items-center justify-content-between">
                <div>
                  <Link to="/compare-product" className="gap-10">
                    <FontAwesomeIcon className="fa-2x" icon={faCodeCompare} />
                    Compare <br /> Products
                  </Link>
                </div>
                <div>
                  <Link to="/wishlist" className="gap-10">
                    <FontAwesomeIcon className="fa-2x" icon={faHeart} />
                    Favourite <br /> Wishlist
                  </Link>
                </div>
                <div>
                  <Link to="/login" className="gap-10">
                    <FontAwesomeIcon className="fa-2x" icon={faUser} />
                    Login <br /> My Account
                  </Link>
                </div>
                <div>
                  <Link to="/cart" className="gap-10">
                    <FontAwesomeIcon className="fa-2x" icon={faCartPlus} />
                    <div className="d-flex flex-column gap-10">
                      <span className="badge bg-white text-dark">0</span>$ 500
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <header className="header-bottom py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="menu-bottom d-flex align-items-center gap-30">
                <div className="dropdown">
                  <button
                    className="btn btn-secondary dropdown-toggle bg-transparent border-0 gap-15"
                    type="button"
                    id="dropdownMenuButton1"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <FontAwesomeIcon icon={faBars} />
                    <span className="me-5 d-inline-block">Shop Categories</span>
                  </button>
                  <ul
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton1"
                  >
                    <li>
                      <Link className="dropdown-item" to="/">
                        Phone
                      </Link>
                    </li>
                    <li>
                      <Link className="dropdown-item" to="/">
                        tablet
                      </Link>
                    </li>
                    <li>
                      <Link className="dropdown-item" to="/">
                        Smart Watch
                      </Link>
                    </li>
                  </ul>
                </div>

                <div className="menu-links">
                  <div className="d-flex align-items-center gap-15">
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/store">Our Store</NavLink>
                    <NavLink to="/blogs">Blogs</NavLink>
                    <NavLink to="/contact">Contact</NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
