import React from "react";
import ReactStars from "react-rating-stars-component";
import SpecialData from "../assets/data/SpecialData";

const SpecialCard = () => {
  return (
    <>
      <div className="row">
        {SpecialData.map((value, index) => {
          return (
            <div className="col-4" key={index}>
              <div className="special-card">
                <div className="percent-off">
                  <span>
                    {((1 - value.discountPrice / value.price) * 100).toFixed(0)}
                    % Off
                  </span>
                </div>
                <div className="d-flex justify-content-between">
                  <div className="card-image">
                    <img className="img-fluid" src={value.imgUrl} alt="" />
                  </div>
                  <div className="special-content">
                    <h6 className="brand">{value.brand}</h6>
                    <h5 className="title">{value.title}</h5>
                    <ReactStars
                      count={5}
                      size={18}
                      value={value.star}
                      edit={false}
                      activeColor="#ffd700"
                    />
                    <div className="discount-price p-1">
                      <p>
                        ${value.discountPrice}
                        &nbsp;<span>${value.price}</span>
                      </p>
                    </div>

                    <div className="discount-till d-flex align-items-center gap-10">
                      <p className="mb-0">
                        <b>5 </b>Days
                      </p>
                      <div className="d-flex gap-10 align-items-center">
                        <span className="badge rounded-circle p-3 bg-warning">
                          1
                        </span>{" "}
                        :
                        <span className="badge rounded-circle p-3 bg-warning">
                          1
                        </span>{" "}
                        :
                        <span className="badge rounded-circle p-3 bg-warning">
                          1
                        </span>
                      </div>
                    </div>
                    <div className="prod-count">
                      <p className="quantity">Product: {value.quantity}</p>
                      <div className="progress">
                        <div
                          className="progress-bar"
                          role="progressbar"
                          aria-valuenow={value?.sold && 0}
                          aria-valuemin="0"
                          aria-valuemax={value.quantity}
                        ></div>
                      </div>
                    </div>
                    <button className="btn-add">Add to Cart</button>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default SpecialCard;
