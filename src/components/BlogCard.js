import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import BlogData from "../assets/data/BlogData";

const BlogCard = () => {
  let location = useLocation();
  const navigate = useNavigate();
  return (
    <>
      <div className="row">
        {BlogData.map((value, index) => {
          return (
            <div
              className={`${
                location.pathname === "/blogs" ? "col-6" : "col-3"
              }`}
              key={index}
              onClick={() => {
                navigate(`/blog/${index}`);
              }}
            >
              <div className="blog-card">
                <div className="card-image">
                  <img className="img-fluid" src={value.imgUrl} alt="" />
                </div>
                <div className="blog-content">
                  <p className="date">{value.date}</p>
                  <h5 className="title">{value.title}</h5>
                  <p className="desc">{value.desc}</p>
                  <button>Read More</button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default BlogCard;
