import {
  faCartShopping,
  faCodeCompare,
  faEye,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import ReactStars from "react-rating-stars-component";
import { Link, useLocation, useNavigate } from "react-router-dom";
import FCollectionData from "../assets/data/FCollectionData";

const ProductCard = (props) => {
  let location = useLocation();
  const navigate = useNavigate();

  const { grid } = props;
  return (
    <>
      <div className="row">
        {FCollectionData.map((value, index) => {
          return (
            <div
              className={`${
                location.pathname === "/store"
                  ? `col-${grid} gr-${grid}`
                  : "col-2"
              }`}
              key={index}
              onClick={() => {
                navigate(`store/product/${index}`);
              }}
            >
              <div className="collection-card position-relative">
                <div className="collection-image">
                  <img className="img-fluid" src={value.imgUrl1} alt="/" />
                  <img className="img-fluid" src={value?.imgUrl2} alt="/" />
                </div>
                <div className="collection-detail">
                  <h6 className="brand">{value.name}</h6>
                  <h5 className="title">{value.title}</h5>
                  <ReactStars
                    count={5}
                    size={18}
                    value={value.star}
                    edit={false}
                    activeColor="#ffd700"
                  />
                  <p
                    className={`description ${
                      grid === 12 ? "d-block" : "d-none"
                    }`}
                  >
                    {value.description}
                  </p>
                  <p className="price">$ {value.price}</p>
                </div>
                <div className="action-bar">
                  <Link>
                    <FontAwesomeIcon className="icon" icon={faHeart} />
                  </Link>
                  <Link>
                    <FontAwesomeIcon
                      className="icon visible"
                      icon={faCartShopping}
                    />
                  </Link>
                  <Link>
                    <FontAwesomeIcon
                      className="icon visible"
                      icon={faCodeCompare}
                    />
                  </Link>
                  <Link>
                    <FontAwesomeIcon className="icon visible" icon={faEye} />
                  </Link>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ProductCard;
