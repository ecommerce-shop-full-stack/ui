import React from "react";

import PopularData from "../assets/data/PopularData";

const PopularCard = () => {
  return (
    <>
      <div className="row popular">
        {PopularData.map((value, index) => {
          return (
            <div className="col-3" key={index}>
              <div className="popular-card position-relative">
                <div className="popular-detail">
                  <h6 className="top">{value.top}</h6>
                  <h5 className="middle">{value.middle}</h5>
                  <p className="bottom">{value.bottom}</p>
                </div>
                <div className="popular-image d-flex justify-content-center">
                  <img className="img-fluid" src={value.imgUrl} alt="/" />
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default PopularCard;
