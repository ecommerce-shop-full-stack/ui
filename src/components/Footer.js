import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import PinterestIcon from "@mui/icons-material/Pinterest";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import { Link } from "react-router-dom";
import chplay from "../assets/images/ch-play.png";
import appstore from "../assets/images/app-store.png";

const Footer = () => {
  return (
    <>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row align-items-center">
            <div className="col-5">
              <div className="footer-top-data d-flex gap-30 align-items-center">
                <FontAwesomeIcon
                  className="fa-2x"
                  icon={faPaperPlane}
                  style={{ color: "white" }}
                />
                <h3 className="mb-0 text-white">Subcribe for more news</h3>
              </div>
            </div>
            <div className="col-7">
              <div className="input-group">
                <input
                  type="text"
                  className="form-control py-1"
                  placeholder="Your email..."
                  aria-label="Your email..."
                  aria-describedby="basic-addon2"
                />
                <span
                  type="button"
                  className="input-group-text p-2"
                  id="basic-addon2"
                >
                  Subcribe
                </span>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <h4>Contact Us</h4>
              <div>
                <p>
                  Demo Store <br />
                  No. 1259 Freedom, New York, 11111 United States <br />
                  +84 909 054 830 <br />
                  Demo@example.gmail.com
                </p>
              </div>
              <div className="row">
                <div className="col-12 social-networks">
                  <div className="social-icons">
                    <Link href="www.twitter.com">
                      <TwitterIcon />
                    </Link>
                  </div>
                  <div className="social-icons">
                    <Link href="www.facebook.com">
                      <FacebookIcon />
                    </Link>
                  </div>
                  <div className="social-icons">
                    <Link href="www.pinterest.com">
                      <PinterestIcon />
                    </Link>
                  </div>
                  <div className="social-icons">
                    <Link href="www.instagram.com">
                      <InstagramIcon />
                    </Link>
                  </div>
                  <div className="social-icons">
                    <Link href="www.youtube.com">
                      <YouTubeIcon />
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2">
              <h4>Information</h4>
              <div className="footer-links">
                <Link to="/privacy-policy">Privacy Policy</Link>
                <Link to="/refund-policy">Refund Policy</Link>
                <Link to="/shipping-policy">Shipping Policy</Link>
                <Link to="/term-condition">Term Of Service</Link>
                <Link to="/blogs">Blogs</Link>
              </div>
            </div>
            <div className="col-2">
              <h4>Account</h4>
              <div className="footer-links">
                <Link to="/search">Search</Link>
                <Link to="/about">About Us</Link>
                <Link to="/faq">Faq</Link>
                <Link to="/contact">Contact</Link>
                <Link to="/">Size Chart</Link>
              </div>
            </div>
            <div className="col-2">
              <h4>Quick Links</h4>
              <div className="footer-links">
                <Link to="/accessories">Accessories</Link>
                <Link to="/laptops">Laptops</Link>
                <Link to="/headphones">Headphones</Link>
                <Link to="/smart-watches">Smart Watches</Link>
                <Link to="/tablets">Tablets</Link>
              </div>
            </div>
            <div className="col-3">
              <h4>Our App</h4>
              <p>
                Download our App and get extra 15% Discount on your first order.
              </p>
              <div className="row">
                <div className="app-download-icon">
                  <img className="ch-play" src={chplay} alt="ch-play" />
                  <img className="app-store" src={appstore} alt="appstore" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <p className="text-center text-white">
                &#169; {new Date().getFullYear()} Powered by Developer
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
