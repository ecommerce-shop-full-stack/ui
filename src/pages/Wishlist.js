import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import cross from "../assets/images/cross.svg";
import CompareProductData from "../assets/data/CompareProductData";

const Wishlist = () => {
  return (
    <>
      <Meta title="Wishlist" />
      <BreadCrumb title="Wishlist" />
      <div className="wishlist-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row justify-content-center">
            {CompareProductData.map((value, index) => {
              return (
                <div className="col-2" key={index}>
                  <div className="wishlist-card position-relative">
                    <img
                      src={cross}
                      alt="cross"
                      className="position-absolute cross img-fluid"
                    />
                    <div className="wishlist-card-image">
                      <img src={value.imgUrl} alt="" className="img-fluid" />
                    </div>
                  </div>
                  <div className="wishlist-description">
                    <h5 className="wishlist-title">{value.title}</h5>
                    <h6 className="price">$ {value.price}</h6>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default Wishlist;
