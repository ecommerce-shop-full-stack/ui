import React from "react";
import { Link } from "react-router-dom";
import Marquee from "react-fast-marquee";

import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import RedeemIcon from "@mui/icons-material/Redeem";
import HeadsetMicIcon from "@mui/icons-material/HeadsetMic";
import DiscountIcon from "@mui/icons-material/Discount";
import PaymentIcon from "@mui/icons-material/Payment";

import mainbanner1 from "../assets/images/banner/main-banner.png";

import BlogCard from "../components/BlogCard";
import ProductCard from "../components/ProductCard";
import SpecialCard from "../components/SpecialCard";

import BrandData from "../assets/data/BrandData";
import CategoriesData from "../assets/data/CategoriesData";
import PopularCard from "../components/PopularCard";
const Home = () => {
  return (
    <>
      <section className="home-wrapper-1 py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-6">
              <div className="main-banner-content p-3">
                <img
                  className="img-fluid rounded-3"
                  src={mainbanner1}
                  alt="main-banner-1"
                />
                <div className="main-banner-content-text">
                  <h4>supercharged for pros.</h4>
                  <h5>Special Sale</h5>
                  <p>
                    From $999.00 or $41.62/mo <br /> for 24 month
                  </p>
                  <Link className="btn">buy now</Link>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="sub-banner">
                <div className="sub-banner-content position-relative p-1">
                  <img
                    className="img-fluid rounded-3"
                    src={mainbanner1}
                    alt="main-banner-1"
                  />
                  <div className="sub-banner-content-text position-absolute">
                    <h4>supercharged for pros.</h4>
                    <h5>Special Sale</h5>
                    <p>
                      From $999.00 or $41.62/mo <br /> for 24 month
                    </p>
                  </div>
                </div>
                <div className="sub-banner-content position-relative p-1">
                  <img
                    className="img-fluid rounded-3"
                    src={mainbanner1}
                    alt="main-banner-1"
                  />
                  <div className="sub-banner-content-text position-absolute">
                    <h4>supercharged for pros.</h4>
                    <h5>Special Sale</h5>
                    <p>
                      From $999.00 or $41.62/mo <br /> for 24 month
                    </p>
                  </div>
                </div>
                <div className="sub-banner-content position-relative p-1">
                  <img
                    className="img-fluid rounded-3"
                    src={mainbanner1}
                    alt="main-banner-1"
                  />
                  <div className="sub-banner-content-text position-absolute">
                    <h4>supercharged for pros.</h4>
                    <h5>Special Sale</h5>
                    <p>
                      From $999.00 or $41.62/mo <br /> for 24 month
                    </p>
                  </div>
                </div>
                <div className="sub-banner-content position-relative p-1">
                  <img
                    className="img-fluid rounded-3"
                    src={mainbanner1}
                    alt="main-banner-1"
                  />
                  <div className="sub-banner-content-text position-absolute">
                    <h4>supercharged for pros.</h4>
                    <h5>Special Sale</h5>
                    <p>
                      From $999.00 or $41.62/mo <br /> for 24 month
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="home-wrapper-2 py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="services">
                <div className="d-flex align-items-center">
                  <LocalShippingIcon className="icons" />
                  <div className="services-text">
                    <h6>Free Shipping</h6>
                    <p>From all orders over $100</p>
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  <RedeemIcon className="icons" />
                  <div className="services-text">
                    <h6>Daily Surprise Offers</h6>
                    <p>Save up to 25% off</p>
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  <HeadsetMicIcon className="icons" />
                  <div className="services-text">
                    <h6>Support 24/7</h6>
                    <p>Shop with an expert</p>
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  <DiscountIcon className="icons" />
                  <div className="services-text">
                    <h6>Affordable Prices</h6>
                    <p>Get Factory direct price</p>
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  <PaymentIcon className="icons" />
                  <div className="services-text">
                    <h6>Secure Payments</h6>
                    <p>100% Protected Payments</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="home-wrapper-2 py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="categories">
                {CategoriesData.map((value, index) => {
                  return (
                    <div className="context" key={index}>
                      <div>
                        <h6>{value.name}</h6>
                        <p>{value.quantity}</p>
                      </div>
                      <img src={value.imgUrl} alt="" />
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="collection-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <h3 className="section-heading">Featured Collection</h3>
          <ProductCard />
        </div>
      </section>
      <section className="special-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <h3 className="section-heading">Special Products</h3>
          <SpecialCard />
        </div>
      </section>
      <section className="popular-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <h3 className="section-heading">Our Popular Products</h3>
          <PopularCard />
        </div>
      </section>
      <section className="marquee-wrapper py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="marquee-inner-wrapper card-wrapper">
                <Marquee>
                  {BrandData.map((value, index) => {
                    return (
                      <div key={index}>
                        <img src={value.imgUrl} alt={value.name} />
                      </div>
                    );
                  })}
                </Marquee>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="blog-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <h3 className="section-heading">Our Lastest News</h3>
          <BlogCard />
        </div>
      </section>
    </>
  );
};

export default Home;
