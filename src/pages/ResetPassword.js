import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";

const ResetPassword = () => {
  return (
    <>
      <Meta title="Reset Password" />
      <BreadCrumb title="Reset Password" />
      <div className="login-wrapper home-wrapper-2 py-5">
        <div className="row">
          <div className="col-12">
            <div className="login-card">
              <h3>Reset Password</h3>
              <p>Set up your new password</p>
              <form action="">
                <div>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="New Password"
                  />
                </div>
                <div>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Confirm New Password"
                  />
                </div>
                <div>
                  <div className="d-flex justify-content-center gap-15 align-items-center">
                    <button className="button">Submit</button>
                    <button className="button">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ResetPassword;
