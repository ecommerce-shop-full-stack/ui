import React, { useState } from "react";
import ReactStars from "react-rating-stars-component";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import ProductCard from "../components/ProductCard";

const SingleProduct = () => {
  const [orderedProduct, setOrderedProduct] = useState(true);
  return (
    <>
      <Meta title="Product Detail" />
      <BreadCrumb title="Product Detail" />
      <div className="main-product-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-6"></div>
            <div className="col-6"></div>
          </div>
          <div className="description-wrapepr py-3 home-wrapper-2">
            <div className="container-xxl">
              <div className="row">
                <div className="col-12">
                  <h4>Description</h4>
                  <div className="bg-white p-3">
                    <p>
                      Contrary to popular belief, Lorem Ipsum is not simply
                      random text. It has roots in a piece of classical Latin
                      literature from 45 BC, making it over 2000 years old.
                      Richard McClintock, a Latin professor at Hampden-Sydney
                      College in Virginia, looked up one of the more obscure
                      Latin words, consectetur, from a Lorem Ipsum passage, and
                      going through the cites of the word in classical
                      literature, discovered the undoubtable source. Lorem Ipsum
                      comes from sections 1.10.32 and 1.10.33 of "de Finibus
                      Bonorum et Malorum" (The Extremes of Good and Evil) by
                      Cicero, written in 45 BC.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="reviews-wrapper py-3 home-wrapper-2">
            <div className="container-xxl">
              <div className="row">
                <div className="col-12">
                  <h4>Reviews</h4>
                  <div className="review-inner-wrapper">
                    <div className="review-head d-flex justify-content-between align-items-end">
                      <div>
                        <h4 className="mb-2">Customer Review</h4>
                        <div className="d-flex align-items-center gap-10">
                          <ReactStars
                            count={5}
                            size={18}
                            value={3}
                            edit={false}
                            activeColor="#ffd700"
                          />
                          <p className="mb-0">Based on 2 Reviews</p>
                        </div>
                      </div>
                      {orderedProduct && (
                        <div>
                          <a
                            className="text-dark text-decoration-underline"
                            href="/"
                          >
                            Write a review
                          </a>
                        </div>
                      )}
                    </div>
                    <div className="review-form py-4">
                      <h4 className="mb-2">Write a Review</h4>
                      <form action="" className="d-flex flex-column gap-15">
                        <ReactStars
                          count={5}
                          size={18}
                          value={0}
                          edit={true}
                          activeColor="#ffd700"
                        />
                        <div>
                          <textarea
                            className="w-100 form-control"
                            cols={30}
                            rows={4}
                            placeholder="Write your comment here"
                          />
                        </div>
                        <div className="button">
                          <button>Submit</button>
                        </div>
                      </form>
                    </div>
                    <div className="reviews">
                      <div className="review">
                        <div className="d-flex gap-10 align-items-center">
                          <h6 className="mb-0">Customer 1</h6>
                          <ReactStars
                            count={5}
                            size={18}
                            value={3}
                            edit={false}
                            activeColor="#ffd700"
                          />
                        </div>
                        <p className="mt-3">
                          There are many variations of passages of Lorem Ipsum
                          available, but the majority have suffered alteration
                          in some form.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row py-5">
            <h3 className="section-heading">Related Product</h3>
            <ProductCard />
          </div>
        </div>
      </div>
    </>
  );
};

export default SingleProduct;
