import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import cross from "../assets/images/cross.svg";

import CompareProductData from "../assets/data/CompareProductData";

const CompareProduct = () => {
  return (
    <>
      <Meta title="Compare Product" />
      <BreadCrumb title="Compare Product" />
      <div className="compare-product-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row justify-content-center">
            {CompareProductData.map((value, index) => {
              return (
                <div className="col-3" key={index}>
                  <div className="compare-product-card position-relative">
                    <img
                      src={cross}
                      alt="cross"
                      className="position-absolute cross img-fluid"
                    />
                    <div className="product-card-image">
                      <img src={value.imgUrl} alt="" className="img-fluid" />
                    </div>
                  </div>
                  <div className="compare-product-description">
                    <h5 className="compare-title">{value.title}</h5>
                    <h6 className="price">$ {value.price}</h6>
                    <div>
                      <div className="details">
                        <h5>Brand</h5>
                        <p>{value.availability}</p>
                      </div>
                      <div className="details">
                        <h5>Type</h5>
                        <p>{value.type}</p>
                      </div>
                      <div className="details">
                        <h5>SKU</h5>
                        <p>{value.sku}</p>
                      </div>
                      <div className="details">
                        <h5>Availability</h5>
                        <p>{value.availability}</p>
                      </div>
                      <div className="details">
                        <h5>Color</h5>
                        <div className="d-flex">
                          <div
                            className="color first-color"
                            style={{ backgroundColor: `${value.firstColor}` }}
                          />
                          <div
                            className="color second-color"
                            style={{ backgroundColor: `${value.secondColor}` }}
                          />
                          <div
                            className="color third-color"
                            style={{ backgroundColor: `${value.thirdColor}` }}
                          />
                        </div>
                      </div>
                      <div className="details">
                        <h5>Size</h5>
                        <p>{value.size}</p>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default CompareProduct;
