import { TextField } from "@mui/material";
import React, { useState } from "react";
import ReactStars from "react-rating-stars-component";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";

import phone from "../assets/images/products/phone.png";
import laptop from "../assets/images/products/laptop.png";

import grid1 from "../assets/images/gr.svg";
import grid2 from "../assets/images/gr2.svg";
import grid3 from "../assets/images/gr3.svg";
import ProductCard from "../components/ProductCard";

const OurStore = () => {
  const [grid, setGrid] = useState(4);

  return (
    <>
      <Meta title={"Our Store"} />
      <BreadCrumb title="Our Store" />
      <div className="store-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <div className="filter-card mb-3">
                <h3 className="filter-title">Shop By Categories</h3>
                <div>
                  <ul>
                    <li>Watch</li>
                    <li>Phone</li>
                    <li>Tivi</li>
                    <li>Speaker</li>
                  </ul>
                </div>
              </div>
              <div className="filter-card mb-3">
                <h3 className="filter-title">Filter By</h3>
                <div>
                  <h5 className="sub-title">Availability</h5>
                  <div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        In Stock (1)
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckChecked"
                      />
                      <label className="form-check-label" htmlFor="">
                        Out of Stock (0)
                      </label>
                    </div>
                  </div>
                  <h5 className="sub-title">Price ($)</h5>
                  <div className="row">
                    <div className="col-6 d-flex align-items-center gap-10">
                      <TextField
                        id="outlined-basic"
                        label="From"
                        variant="outlined"
                      />
                    </div>
                    <div className="col-6 d-flex align-items-center gap-10">
                      <TextField
                        id="outlined-basic"
                        label="To"
                        variant="outlined"
                      />
                    </div>
                  </div>
                  <h5 className="sub-title">Color</h5>
                  <div className="">
                    <ul className="color-list">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                    </ul>
                  </div>
                  <h5 className="sub-title">Size</h5>
                  <div className="">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        S (1)
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        M (1)
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        L (1)
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        XL (1)
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label" htmlFor="">
                        XXL (1)
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="filter-card mb-3">
                <h3 className="filter-title">Product Tags</h3>
                <div className="product-tags">
                  <span className="">Headphone</span>
                  <span className="">Phone</span>
                  <span className="">Laptop</span>
                  <span className="">Smart Watch</span>
                  <span className="">Play Station</span>
                </div>
              </div>
              <div className="filter-card mb-3">
                <h3 className="filter-title">Random Product</h3>
                <div className="random-product">
                  <div className="d-flex gap-15">
                    <div className="w-25">
                      <img className="img-fluid" src={phone} alt="/" />
                    </div>
                    <div className="w-75">
                      <h5>iPhone 14 Pro Max 512G</h5>
                      <ReactStars
                        count={5}
                        size={18}
                        value={3}
                        edit={false}
                        activeColor="#ffd700"
                      />
                      <p>$ 800</p>
                    </div>
                  </div>
                </div>
                <div className="random-product">
                  <div className="d-flex gap-15">
                    <div className="w-25">
                      <img className="img-fluid" src={laptop} alt="/" />
                    </div>
                    <div className="w-75">
                      <h5>Alien Ware TTG</h5>
                      <ReactStars
                        count={5}
                        size={18}
                        value={3}
                        edit={false}
                        activeColor="#ffd700"
                      />
                      <p>$ 1.800</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-9">
              <div className="filter-sort-grid">
                <div className="d-flex justify-content-between align-items-center">
                  <div className="d-flex align-items-center gap-10">
                    <p className="mb-0" style={{ width: "100px" }}>
                      Sort By:
                    </p>
                    <select
                      name=""
                      className="form-control form-select"
                      defaultValue={"best-selling"}
                    >
                      <option value="manual" disabled>
                        Sort
                      </option>
                      <option value="best-selling">Best Selling</option>
                      <option value="price-ascending">
                        Price, low to high
                      </option>
                      <option value="price-descending">
                        Price, high to low
                      </option>
                    </select>
                  </div>
                  <div className="d-flex align-items-center gap-10">
                    <p className="total-products mb-0">21 Products</p>
                    <div className="d-flex gap-10 align-items-center grid">
                      <img
                        onClick={() => {
                          setGrid(4);
                        }}
                        className="d-block img-fluid"
                        src={grid3}
                        alt="grid"
                      />
                      <img
                        onClick={() => {
                          setGrid(6);
                        }}
                        className="d-block img-fluid"
                        src={grid2}
                        alt="grid"
                      />
                      <img
                        onClick={() => {
                          setGrid(12);
                        }}
                        className="d-block img-fluid"
                        src={grid1}
                        alt="grid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="product-list">
                <ProductCard grid={grid} />
                <ProductCard grid={grid} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OurStore;
