import React from "react";
import BlogCard from "../components/BlogCard";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";

const Blog = () => {
  return (
    <>
      <Meta title={"Blog"} />
      <BreadCrumb title="Blog" />
      <div className="blog-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <div className="filter-card mb-3">
                <h3 className="filter-title">Shop By Categories</h3>
                <div>
                  <ul>
                    <li>Watch</li>
                    <li>Phone</li>
                    <li>Tivi</li>
                    <li>Speaker</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-9">
              <BlogCard />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Blog;
