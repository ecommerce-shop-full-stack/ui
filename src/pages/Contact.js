/* eslint-disable jsx-a11y/iframe-has-title */
import {
  faAddressCard,
  faInfo,
  faMailBulk,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";

const Contact = () => {
  return (
    <>
      <Meta title={"Contact"} />
      <BreadCrumb title="Contact" />
      <div className="contact-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d501726.54072405986!2d106.36556771075256!3d10.754618136432917!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529292e8d3dd1%3A0xf15f5aad773c112b!2sHo%20Chi%20Minh%20City%2C%20Vietnam!5e0!3m2!1sen!2s!4v1693380405231!5m2!1sen!2s"
                className="border-0 w-100"
                width={600}
                height={450}
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              ></iframe>
            </div>
            <div className="col-12 mt-3">
              <div className="contact-inner-wrapper d-flex justify-content-between">
                <div>
                  <h3 className="contact-title mb-4">Contact</h3>
                  <form action="" className="d-flex flex-column gap-15">
                    <div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Name"
                      />
                    </div>
                    <div>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Email"
                      />
                    </div>
                    <div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Mobile Number"
                      />
                    </div>
                    <div>
                      <textarea
                        className="w-100 form-control"
                        cols={30}
                        rows={4}
                        placeholder="Comments"
                      />
                    </div>
                    <div className="button">
                      <button>Submit</button>
                    </div>
                  </form>
                </div>
                <div>
                  <h3 className="contact-title mb-4">Get in touch with us</h3>
                  <div className="d-flex flex-column gap-15 align-items-start">
                    <div className="d-flex align-items-center">
                      <FontAwesomeIcon
                        className="contact-icon"
                        icon={faAddressCard}
                      />
                      <p className="mb-0">
                        33 New Montgomery St.Ste 750 San Francisco, CA, USA
                        94105
                      </p>
                    </div>
                    <div className="d-flex align-items-center">
                      <FontAwesomeIcon
                        className="contact-icon"
                        icon={faPhone}
                      />
                      <p className="mb-0">(+91) 7-723-4608</p>
                    </div>
                    <div className="d-flex align-items-center">
                      <FontAwesomeIcon
                        className="contact-icon"
                        icon={faMailBulk}
                      />
                      <p className="mb-0">demo@company.com</p>
                    </div>
                    <div className="d-flex align-items-center">
                      <FontAwesomeIcon className="contact-icon" icon={faInfo} />
                      <p className="mb-0">Monday - Friday (From 10AM - 6PM)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
