import React from "react";
import { Link, useParams } from "react-router-dom";
import BreadCrumb from "../components/BreadCrumb";
import Meta from "../components/Meta";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import BlogData from "../assets/data/BlogData";

const SingleBlog = () => {
  let { id } = useParams();

  return (
    <>
      <Meta title={BlogData[id].title} />
      <BreadCrumb title={BlogData[id].title} />
      <div className="blog-wrapper home-wrapper-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="single-blog-card">
                <h3 className="title">{BlogData[id].title}</h3>
                <p className="date">{BlogData[id].date}</p>
                <img className="img-fluid" src={BlogData[id].imgUrl} alt="/" />
                <p className="text">{BlogData[id].text}</p>
              </div>
              <Link className="mb-5 m-4" to="/blogs">
                <ArrowBackIcon /> &nbsp; Go back to Blogs
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SingleBlog;
